package com.epam.training.sportsbetting;

import com.epam.training.sportsbetting.domain.*;
import com.epam.training.sportsbetting.service.BettingServices;
import com.epam.training.sportsbetting.service.SportsBettingService;
import com.epam.training.sportsbetting.view.Print;
import com.epam.training.sportsbetting.view.View;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class App {


    public static void main(String[] args) {


        Print print = new Print();
        BettingServices bettingService = new BettingServices();
        Player player = print.readPlayerdata();
        print.printWelcomeMessage(player);
        print.printBalance(player);
        List<SportEvent> sportEvents = bettingService.findAllSportEvents();
        String selectSportevent;
        Scanner sc = new Scanner(System.in);
        List<Wager> wagers = new ArrayList<Wager>();

        do {
            print.printOutcomeOdds(sportEvents);
            selectSportevent = sc.nextLine();
            if(selectSportevent.contains("q"))
                break;
            OutcomeOdd outcomeOdd = print.selectOutcomeOdds(sportEvents,Integer.parseInt(selectSportevent));
            BigDecimal amount = print.readWagerAmount();
            BigDecimal wagerLost = BigDecimal.valueOf(1000);
            int loseWager = player.getBalance().compareTo(wagerLost);
            if(loseWager == -1) {
                wagers.add(new Wager(player,outcomeOdd,player.getCurrency(),amount,LocalDateTime.now(),
                        true,true));
                player.setBalance(player.getBalance().add(amount.multiply(outcomeOdd.getValue())));
            }
            else {
                wagers.add(new Wager(player, outcomeOdd, player.getCurrency(), amount, LocalDateTime.now(),
                        true, false));

            }
            //print.printWagersaved();
            System.out.println(player.getBalance());



        }while(selectSportevent.indexOf('q')==-1);
        print.printResults(player,wagers);



    }


}
