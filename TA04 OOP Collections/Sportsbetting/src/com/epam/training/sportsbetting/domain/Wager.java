package com.epam.training.sportsbetting.domain;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Wager {

    Player player;
    OutcomeOdd outcomeOdd;
    Currency currency;
    BigDecimal amount;
    LocalDateTime timeStampCreated;
    boolean processed;
    boolean win;

    public Wager(Player player, OutcomeOdd outcomeOdd, Currency currency, BigDecimal amount,
                 LocalDateTime timeStampCreated, boolean processed, boolean win) {
        this.player = player;
        this.outcomeOdd = outcomeOdd;
        this.currency = currency;
        this.amount = amount;
        this.timeStampCreated = timeStampCreated;
        this.processed = processed;
        this.win = win;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public OutcomeOdd getOutcomeOdd() {
        return outcomeOdd;
    }

    public void setOutcomeOdd(OutcomeOdd outcomeOdd) {
        this.outcomeOdd = outcomeOdd;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public LocalDateTime getTimeStampCreated() {
        return timeStampCreated;
    }

    public void setTimeStampCreated(LocalDateTime timeStampCreated) {
        this.timeStampCreated = timeStampCreated;
    }

    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    public boolean isWin() {
        return win;
    }

    public void setWin(boolean win) {
        this.win = win;
    }
}
