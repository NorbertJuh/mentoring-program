package com.epam.training.sportsbetting.domain;

import java.time.LocalDateTime;

public class TennisSportEvent extends SportEvent{

    public TennisSportEvent(String title, LocalDateTime startDate, LocalDateTime endDate, Bet bet, Result result) {
        super(title, startDate, endDate, bet, result);
    }
}
