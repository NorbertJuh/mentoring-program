package com.epam.training.sportsbetting.domain;

import java.math.MathContext;

public class Outcome {

    String description;
    OutcomeOdd outcomeOdd;

    public Outcome(String description, OutcomeOdd outcomeOdd) {
        this.description = description;
        this.outcomeOdd = outcomeOdd;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public OutcomeOdd getOutcomeOdd() {
        return outcomeOdd;
    }

    public void setOutcomeOdd(OutcomeOdd outcomeOdd) {
        this.outcomeOdd = outcomeOdd;
    }

   /* @Override
    public String toString() {

        return " Outcome: " + description + '\'' +
                ", Actual odd: " + outcomeOdd.getValue();
    }*/
}
