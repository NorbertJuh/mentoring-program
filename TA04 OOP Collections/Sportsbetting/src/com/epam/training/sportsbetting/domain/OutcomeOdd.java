package com.epam.training.sportsbetting.domain;

import java.math.BigDecimal;
import java.math.MathContext;
import java.time.LocalDateTime;

public class OutcomeOdd {


    BigDecimal value;
    LocalDateTime validFrom;
    LocalDateTime validUntil;
    //Outcome outcome;

    public OutcomeOdd(BigDecimal value, LocalDateTime validFrom, LocalDateTime validUntil) {
        MathContext m;
        m = new MathContext(2);
        value = value.round(m);
        this.value = value;
        this.validFrom = validFrom;
        this.validUntil = validUntil;

    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public LocalDateTime getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(LocalDateTime validFrom) {
        this.validFrom = validFrom;
    }

    public LocalDateTime getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(LocalDateTime validUntil) {
        this.validUntil = validUntil;
    }


}