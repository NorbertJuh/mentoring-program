package com.epam.training.sportsbetting.domain;

import java.util.List;

public class Result {

    List<Outcome> outcomeList;

    public Result(List<Outcome> outcomeList) {
        this.outcomeList = outcomeList;
    }

    public List<Outcome> getOutcomeList() {
        return outcomeList;
    }

    public void setOutcomeList(List<Outcome> outcomeList) {
        this.outcomeList = outcomeList;
    }
}
