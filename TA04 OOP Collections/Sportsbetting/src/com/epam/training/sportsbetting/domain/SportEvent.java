package com.epam.training.sportsbetting.domain;

import java.time.LocalDateTime;

public abstract class SportEvent {

    String title;
    LocalDateTime startDate;
    LocalDateTime endDate;
    Bet bet;
    Result result;

    public SportEvent(String title, LocalDateTime startDate, LocalDateTime endDate, Bet bet, Result result) {
        this.title = title;
        this.startDate = startDate;
        this.endDate = endDate;
        this.bet = bet;
        this.result = result;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public Bet getBet() {
        return bet;
    }

    public void setBet(Bet bet) {
        this.bet = bet;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }
}
