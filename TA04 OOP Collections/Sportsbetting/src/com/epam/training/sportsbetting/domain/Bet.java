package com.epam.training.sportsbetting.domain;

import java.util.List;

public class Bet{

    String description;
    BetType betType;
    List<Outcome> outcome;


    public Bet(String description, BetType betType, List<Outcome> outcome) {
        this.description = description;
        this.betType = betType;
        this.outcome = outcome;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BetType getBetType() {
        return betType;
    }

    public void setBetType(BetType betType) {
        this.betType = betType;
    }

    public List<Outcome> getOutcome() {
        return outcome;
    }

    public void setOutcome(List<Outcome> outcome) {
        this.outcome = outcome;
    }
}
