package com.epam.training.sportsbetting.service;

import com.epam.training.sportsbetting.domain.*;
import com.epam.training.sportsbetting.view.Print;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BettingServices implements SportsBettingService{

    Player player = new Player("","","",0,null,null, null);
    Print print = new Print();

    @Override
    public void savePlayer(Player player) {

        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();
        player.setName(name);


    }

    @Override
    public Player findPlayer() {
        System.out.println("Enter the player name: ");
        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();
        if(name == player.getName())
            return player;
        return null;
    }

    @Override
    public List<SportEvent> findAllSportEvents() {
        List<SportEvent> sportevents = new ArrayList<SportEvent>();

        OutcomeOdd outcomeOdd1 = new OutcomeOdd(new BigDecimal(2),
                LocalDateTime.of(2020,5,15,17,0)
                        ,LocalDateTime.of(2020,5,16,17,0));
        OutcomeOdd outcomeOdd2 = new OutcomeOdd(new BigDecimal(2.6),
                LocalDateTime.of(2020,5,15,17,0)
                ,LocalDateTime.of(2020,5,16,17,0));
        OutcomeOdd outcomeOdd3 = new OutcomeOdd(new BigDecimal(3.3),
                LocalDateTime.of(2020,5,15,17,0)
                ,LocalDateTime.of(2020,5,16,17,0));
        OutcomeOdd outcomeOdd4 = new OutcomeOdd(new BigDecimal(1.5),
                LocalDateTime.of(2020,5,15,17,0)
                ,LocalDateTime.of(2020,5,16,17,0));
        OutcomeOdd outcomeOdd5 = new OutcomeOdd(new BigDecimal(2.7),
                LocalDateTime.of(2020,5,15,17,0)
                ,LocalDateTime.of(2020,5,16,17,0));
        OutcomeOdd outcomeOdd6 = new OutcomeOdd(new BigDecimal(1.8),
                LocalDateTime.of(2020,5,15,17,0)
                ,LocalDateTime.of(2020,5,16,17,0));

        Outcome outcome1 = new Outcome("Yes" , outcomeOdd1);
        Outcome outcome2 = new Outcome("No" , outcomeOdd2);
        Outcome outcome3 = new Outcome("3 or more" , outcomeOdd5);
        Outcome outcome4 = new Outcome("less than 3" , outcomeOdd1);
        Outcome outcome5 = new Outcome("Nadal",outcomeOdd4);
        Outcome outcome6 = new Outcome("Federer",outcomeOdd1);


        List<Outcome> outcomes = new ArrayList<Outcome>();
        outcomes.add(outcome1);
        outcomes.add(outcome2);

        List<Outcome> outcomes2 = new ArrayList<Outcome>();
        outcomes2.add(outcome3);
        outcomes2.add(outcome4);

        List<Outcome> outcomes3 = new ArrayList<Outcome>();
        outcomes3.add(outcome5);
        outcomes3.add(outcome6);

        List<Outcome> outcomes4 = new ArrayList<Outcome>();
        outcomes4.add(outcome3);
        outcomes4.add(outcome4);


        Bet bet1 = new Bet("player Oliver Giroud score", BetType.PLAYERS_SCORE,outcomes);
        Bet bet2 = new Bet("number of scored goals",BetType.GOALS,outcomes2);
        Bet bet3 = new Bet("winner",BetType.WINNER,outcomes3);
        Bet bet4 = new Bet("number of sets",BetType.NUMBER_OF_SETS,outcomes4);

        Result result = new Result(outcomes);

        SportEvent sportEvent1 = new FootballSportEvent("Arsenal vs Chelsea",
                LocalDateTime.of(2020,5,15,12,0)
                ,LocalDateTime.of(2020,5,16,17,0),bet1,result);
        SportEvent sportEvent2 = new FootballSportEvent("Arsenal vs Chelsea",
                LocalDateTime.of(2020,5,15,12,0)
                ,LocalDateTime.of(2020,5,16,17,0),bet2,result);
        SportEvent sportEvent3 = new TennisSportEvent("Federer vs Nadal",
                LocalDateTime.of(2020,5,15,12,0)
                ,LocalDateTime.of(2020,5,16,17,0),bet3,result);
        SportEvent sportEvent4 = new TennisSportEvent("Federer vs Nadal",
                LocalDateTime.of(2020,5,15,12,0)
                ,LocalDateTime.of(2020,5,16,17,0),bet4,result);

        sportevents.add(sportEvent1);
        sportevents.add(sportEvent2);
        sportevents.add(sportEvent3);
        sportevents.add(sportEvent4);


        return sportevents;

    }

    @Override
    public void savewager(Wager wager) {
        //print.readWagerAmount();

    }

    @Override
    public List<Wager> findAllWagers() {

        findAllSportEvents();
        List<Wager> wagers = new ArrayList<Wager>();

        //Wager wager1 = new Wager(player,)
        return null;
    }

    @Override
    public void calculateResults() {

    }
}
