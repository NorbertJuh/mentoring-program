package com.epam.training.sportsbetting.service;

import com.epam.training.sportsbetting.domain.Player;
import com.epam.training.sportsbetting.domain.SportEvent;
import com.epam.training.sportsbetting.domain.Wager;

import java.util.List;

public interface SportsBettingService {

    public void  savePlayer(Player player);
    public Player findPlayer();
    public List<SportEvent> findAllSportEvents();
    public void savewager(Wager wager);
    public List<Wager> findAllWagers();
    public void calculateResults();

}
