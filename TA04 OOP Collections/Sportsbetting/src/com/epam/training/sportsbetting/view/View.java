package com.epam.training.sportsbetting.view;

import com.epam.training.sportsbetting.domain.OutcomeOdd;
import com.epam.training.sportsbetting.domain.Player;
import com.epam.training.sportsbetting.domain.SportEvent;
import com.epam.training.sportsbetting.domain.Wager;

import java.math.BigDecimal;
import java.util.List;

public interface View {

    public Player readPlayerdata();
    public void printWelcomeMessage(Player player);
    public void printBalance(Player player);
    public void printOutcomeOdds(List<SportEvent> sportEvents);
    public OutcomeOdd selectOutcomeOdds(List<SportEvent> sportEvents, int index);
    public BigDecimal readWagerAmount();
    public void printWagersaved(Wager wager);
    public void printNotEnoughBalance(Player player);
    public void printResults(Player player,List<Wager> wagers);

}
