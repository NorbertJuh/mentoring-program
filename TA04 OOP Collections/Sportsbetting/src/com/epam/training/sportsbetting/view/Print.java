package com.epam.training.sportsbetting.view;

import com.epam.training.sportsbetting.domain.*;
import com.epam.training.sportsbetting.service.BettingServices;

import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;

public class Print implements View{
    Player player = new Player("","","",0,null,null, null);
    @Override
    public Player readPlayerdata() {


        BettingServices bettingService = new BettingServices();

        System.out.println("What is your name? ");
        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();
        player.setName(name);
        //bettingService.savePlayer(player);

        System.out.println("How much money do you have (more than 0)?");
        Scanner sc2 = new Scanner(System.in);
        BigDecimal money = sc2.nextBigDecimal();
        player.setBalance(money);

        System.out.println("What is your currency? (HUF, EUR or USD)");
        Scanner sc3 = new Scanner(System.in);
        String currency = sc3.nextLine();
        currency.toUpperCase();
        if (currency.indexOf("HUF") != -1)
            player.setCurrency(Currency.HUF);
        if (currency.indexOf("EUR") != -1)
            player.setCurrency(Currency.EUR);
        if (currency.indexOf("USD") != -1)
            player.setCurrency(Currency.USD);

        return player;
    }

    @Override
    public void printWelcomeMessage(Player player) {
        System.out.println("Welcome " + player.getName() + "!");

    }

    @Override
    public void printBalance(Player player) {
        System.out.println("Your balance is " + player.getBalance() + " " + player.getCurrency());
    }

    @Override
    public void printOutcomeOdds(List<SportEvent> sportEvents) {
        System.out.println("What are you want to bet on? (choose a number or press q for quit)");
        AtomicInteger i = new AtomicInteger(1);
        sportEvents.forEach(actualSportevent ->
                {
                    System.out.println(i + ": Sport event: " + actualSportevent.getTitle() + " (start: "
                            + actualSportevent.getStartDate() + "), Bet: " + actualSportevent.getBet().getDescription() +
                            ", Outcome: " + actualSportevent.getBet().getOutcome().get(0).getDescription() +
                            ", Actual odd: " +  actualSportevent.getBet().getOutcome().get(0).getOutcomeOdd().getValue() +
                            ", Valid between " + actualSportevent.getBet().getOutcome().get(1).getOutcomeOdd().getValidFrom() +
                            " and " + actualSportevent.getBet().getOutcome().get(1).getOutcomeOdd().getValidUntil() +".");
                    i.getAndIncrement();
                }
                );
        /*for (int i = 1; i < sportEvents.size(); i++) {
            System.out.println(i + sportEvents[i]);
        }*/

    }

    @Override
    public OutcomeOdd selectOutcomeOdds(List<SportEvent> sportEvents, int index) {

            return sportEvents.get(index-1).getBet().getOutcome().get(0).getOutcomeOdd();

    }

    @Override
    public BigDecimal readWagerAmount() {
        System.out.println("What amount do you wish to bet on it?");
        Scanner sc = new Scanner(System.in);
        BigDecimal wagerAmount = sc.nextBigDecimal();
        int notNegativeAmount = player.getBalance().compareTo(wagerAmount);
        if(notNegativeAmount == -1) {
            printNotEnoughBalance(player);
            return null;
        }
        player.setBalance(player.getBalance().subtract(wagerAmount));

        return wagerAmount;
    }

    @Override
    public void printWagersaved(Wager wager) {
      System.out.println("Wager 'player Oliver Giroud score=1' of Arsenal vs Chelsea"
              + " [odd: " + wager.getOutcomeOdd().getValue() + ", amount: " + wager.getAmount() + "] saved!)");
    }

    @Override
    public void printNotEnoughBalance(Player player) {
        System.out.println("You don't have enough money, your balance is " + player.getBalance() +
                " " + player.getCurrency());
    }

    @Override
    public void printResults(Player player, List<Wager> wagers) {
        wagers.forEach(wager -> {
            System.out.println("Wager " +
            "[odd: " + wager.getOutcomeOdd().getValue() + " amount: " + wager.getAmount() + " " + wager.getCurrency()
                    + "], win: " + wager.isWin());
                }

        );

        System.out.println("Your new balance is " + player.getBalance() + " " + player.getCurrency() + ".");

    }
}
