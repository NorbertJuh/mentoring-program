package com.company;

public class Main {

    public static void main(String[] args) {

        int number = 1;

        if (number > 3999 || number < 1) {
            System.out.println("Please write a number between 1 and 3999");
        }
        else {
            System.out.print("The converted number is: ");
            while (number >= 1000) {
                System.out.print("M");
                number -= 1000;
            }
            while (number >= 900) {
                System.out.print("CM");
                number -= 900;
            }
            while (number >= 500) {
                System.out.print("D");
                number -= 500;
            }
            while (number >= 400) {
                System.out.print("CD");
                number -= 400;
            }
            while (number >= 100) {
                System.out.print("C");
                number -= 100;
            }
            while (number >= 90) {
                System.out.print("XC");
                number -= 90;
            }
            while (number >= 50) {
                System.out.print("L");
                number -= 50;
            }
            while (number >= 40) {
                System.out.print("XL");
                number -= 40;
            }
            while (number >= 10) {
                System.out.print("X");
                number -= 10;
            }
            while (number >= 9) {
                System.out.print("IX");
                number -= 9;
            }
            while (number >= 5) {
                System.out.print("V");
                number -= 5;
            }
            while (number >= 4) {
                System.out.print("IV");
                number -= 4;
            }
            while (number >= 1) {
                System.out.print("I");
                number -= 1;
            }
        }
    }
}
