/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.toto.service;


import com.epam.training.toto.domain.Round;
import com.epam.training.toto.domain.Hit;
import com.epam.training.toto.domain.Outcome;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.text.SimpleDateFormat;  
import java.time.LocalDate;

/**
 *
 * @author jnorb
 */
public class Totoservice {
    
public List <Round> csvReader(){   
    
    String csvFile = "toto.csv";       
        
        List<Round> content = new ArrayList<Round>();
        try{File inputF = new File(csvFile);
                InputStream inputFS = new FileInputStream(inputF);
                BufferedReader br = new BufferedReader(new InputStreamReader(inputFS));
            content = br.lines().map(mapToItem).collect(Collectors.toList());
        
            br.close();
        } catch (IOException ex) {
        Logger.getLogger(Totoservice.class.getName()).log(Level.SEVERE, null, ex);
    }
        
        return content;
}
        
  private Function<String, Round> mapToItem = (line) -> {
        String[] data = line.split(";");      
        
        Round item = new Round();
        item.setYear(Integer.parseInt(data[0]));
        item.setWeek(Integer.parseInt(data[1]));
        if(data[2].contains("-"))
            item.setRoundOfWeek(1);
        else
            item.setRoundOfWeek(Integer.parseInt(data[2]));
        
        if(data[3].length() > 1){
        String[] date = data[3].split("\\.");    
        item.setDate(LocalDate.parse(date[0] + "-" + date[1] + "-" + date[2]));
        }
        else{
        item.setDate(LocalDate.now());
        }
        
        List <Hit> hits = new ArrayList<Hit>();
        hits.add(new Hit(14,Integer.parseInt(data[4]),CleanPrize(data[5])));
        hits.add(new Hit(13,Integer.parseInt(data[6]),CleanPrize(data[7])));
        hits.add(new Hit(12,Integer.parseInt(data[8]),CleanPrize(data[9])));
        hits.add(new Hit(11,Integer.parseInt(data[10]),CleanPrize(data[11])));
        hits.add(new Hit(10,Integer.parseInt(data[12]),CleanPrize(data[13])));
             
        item.setHits(hits);
        
        List <Outcome> outcomes = new ArrayList();
        for(int i=14;i<28;i++){
        if(data[i].contains("1"))
            outcomes.add(Outcome._1);
        else if(data[i].contains("2"))
            outcomes.add(Outcome._2);
        else if(data[i].contains("X"))
            outcomes.add(Outcome.X);
        else if(data[i].contains("x"))
            outcomes.add(Outcome.X);
            }

        item.setOutcomes(outcomes);
        //System.out.println(data[14]);
        
        int ones = 0;
        int twos = 0;
        int xes = 0;
        
        for(Outcome num: outcomes){
            if(num == Outcome._1)
                ones++;
            if(num == Outcome._2)
                twos++;
            if(num == Outcome.X)
                xes++;
        }
        int maxPoint = 14;
        int onespercent = 0;
        int twospercent = 0;
        int xespercent = 0;
        onespercent =ones*100/maxPoint;
        twospercent =twos*100/maxPoint;
        xespercent =xes*100/maxPoint;
        //System.out.println(data[3] + "Statistics: team #1 won: " + onespercent + " %" +
                //", team #2 won: " + twospercent + "%, draw: " + xespercent + " %");
        
        
        
        return item;
    }; 
  
  private int CleanPrize(String prize){
      String pom;
      pom = prize.replace("Ft","");
      pom = pom.replaceAll("\\s", "");
      
      return Integer.parseInt(pom);
  }
  
  
  
}
