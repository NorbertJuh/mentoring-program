/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.toto.domain;

/**
 *
 * @author jnorb
 */
public class Hit {

int HitCount;
int numberOfWagers;
int prize;

    public Hit(int HitCount, int numberOfWagers, int prize) {
        this.HitCount = HitCount;
        this.numberOfWagers = numberOfWagers;
        this.prize = prize;
    }

    public int getHitCount() {
        return HitCount;
    }

    public int getNumberOfWagers() {
        return numberOfWagers;
    }

    public int getPrize() {
        return prize;
    }

    public void setHitCount(int HitCount) {
        this.HitCount = HitCount;
    }

    public void setNumberOfWagers(int numberOfWagers) {
        this.numberOfWagers = numberOfWagers;
    }

    public void setPrize(int prize) {
        this.prize = prize;
    }


    
    
}
