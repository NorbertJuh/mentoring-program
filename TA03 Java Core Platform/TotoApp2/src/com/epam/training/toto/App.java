/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.toto;
import java.time.LocalDate;
import java.util.*;

import com.epam.training.toto.domain.Hit;
import com.epam.training.toto.domain.Outcome;
import com.epam.training.toto.domain.Round;
import com.epam.training.toto.service.Totoservice;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 *
 * @author jnorb
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        
    Totoservice ts = new Totoservice();
    List <Round> allData = ts.csvReader();
    allData.stream();
        int[] maxPrize = {0};
        List<Outcome> results1 = new ArrayList<Outcome>();
        List<Outcome> results2 = new ArrayList<Outcome>();
        List<Outcome> resultsX = new ArrayList<Outcome>();
    allData.forEach(actualRound->{
        List <Outcome> outcomes = new ArrayList();
        outcomes.addAll(actualRound.getOutcomes());
        for(Outcome num: outcomes){
            
            if(num == Outcome._1)
                results1.add(num);           
            if(num == Outcome._2)
                results2.add(num);
            if(num == Outcome.X)
                resultsX.add(num);
        }
         
        
        
        if(actualRound.getHits().get(0).getPrize() > maxPrize[0])
            maxPrize[0] = actualRound.getHits().get(0).getPrize();
        
        //int var = allData.stream().max(Integer::compare).get();
         
        
    });
    
        String decimalPattern = "###,###,### Ft";
        
        DecimalFormat df = new DecimalFormat(decimalPattern);
       
        
        System.out.println("The largest prize ever recorded: " + df.format(maxPrize[0]));





    double sumOfResults = results1.size() + results2.size() + resultsX.size();
        double onespercent = 0;
        double twospercent = 0;
        double xespercent = 0;
        onespercent = results1.size()*100/sumOfResults;
        twospercent =results2.size()*100/sumOfResults;
        xespercent =resultsX.size()*100/sumOfResults;
        System.out.println("Statistics: team #1 won: " + String.format("%.2f", onespercent) + " %" +
                ", team #2 won: " + String.format("%.2f", twospercent) + "%, draw: " +
                String.format("%.2f", xespercent) + " %");



        System.out.println("Enter date: ");
        Scanner sc = new Scanner(System.in);
        String userDate = sc.next();

        if (!userDate.matches("^[0-9,.]*$"))
            throw new IllegalArgumentException("Only numbers in the input. ");

        String[] dateArray = userDate.split("\\.");
        LocalDate dateSearch = LocalDate.of(Integer.parseInt(dateArray[0]),Integer.parseInt(dateArray[1]),
                Integer.parseInt(dateArray[2]));

        List<Outcome> outcomes = new ArrayList<Outcome>();
        List<Hit> hits = new ArrayList<Hit>();

        allData.forEach(actualRound->{
            if(dateSearch.isEqual(actualRound.getDate())) {
                System.out.println(actualRound.getOutcomes().toString());
                outcomes.addAll(actualRound.getOutcomes());
                hits.addAll(actualRound.getHits());
                hits.forEach(actualHits->{
                    //System.out.println(actualHits.getHitCount());
                    //System.out.println(actualHits.getNumberOfWagers());
                    //System.out.println(actualHits.getPrize());
                });
            }
            });

        System.out.println(outcomes.toString());


        final int[] result = {0};
        int prize = 0;
        System.out.println("Enter outcomes: " );
        Scanner sc2 = new Scanner(System.in);
        String userOutcomes = sc2.next();

        if (!userOutcomes.matches("^[1-2,X]*$"))
            throw new IllegalArgumentException("Only 1,2,X in the input. ");


        char[] singleOutcome = userOutcomes.toCharArray();
        final int[] arrayIndex = {0};

        outcomes.forEach(actualOutcome-> {
            if(actualOutcome.toString().indexOf(singleOutcome[arrayIndex[0]])!=-1) {
                //System.out.println(actualOutcome.toString());
                result[0]++;
            }
            arrayIndex[0]++;

        });

        if(result[0]==10)
            prize=hits.get(4).getPrize();
        else if(result[0]==11)
            prize=hits.get(3).getPrize();
        else if(result[0]==12)
            prize=hits.get(2).getPrize();
        else if(result[0]==13)
            prize=hits.get(1).getPrize();
        else if(result[0]==14)
            prize=hits.get(0).getPrize();

        
        System.out.println("Result: " + result[0] + ", amount: " + df.format(prize));
    
        }
    
    
}           
                 

